export function test() {
    new Vue({
        el: '#tests',
        data: {
            testName: 'YO INVOKED',
            message: 'hello Vues!',
            array: ["Sue", "Tom", "Henry"],
            test: false,
            buttonStatus: false
        },
        methods: {
            addName() {
                this.array.push(this.newName);
                this.newName = '';
                this.changeColor();
            },
            changeColor() {
                this.test = true;
            }
        },
        computed: {
            reversedMessage() {
                return this.message.split('').reverse().join('')
            }
        }
    })

}
