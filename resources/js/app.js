import Vue from 'vue'
import { MdButton, MdContent, MdTabs, MdBottomBar, MdIcon} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)
Vue.use(MdBottomBar)
Vue.use(MdIcon)

import {test} from './test.js';
import main from './main.vue';

// So just import a BarRouter and then invoke using vue render.

test();

new Vue({
    render: h => h(main),
}).$mount('#testfinal')

require('./bootstrap');


